#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

PROGNAME=$(basename "$0")

SOURCE=$1
TARGET=$2
CHANGESET=${3:-"HEAD"}

if [ ! -e "$SOURCE" ] || [ ! "$TARGET" ] || [ "$SOURCE" = '--help' ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME <source repository> <target repository> [ <changeset> ]

Unpack the source repository in the target location, updated to the indicated
changeset/version (or to the HEAD changeset if not specified).
EOF
    exit 1
fi

# Get the location of the target repository and its name.

PARENT=$(dirname "$TARGET")
DIRNAME=$(basename "$TARGET")

if [ ! -e "$PARENT" ] ; then
    mkdir -p "$PARENT"
fi

# Make sure that absolute paths are used when changing directories.

PARENT=$(realpath "$PARENT")

# Archive the repository for a given version and then unpack the archive.

cd "$SOURCE"
git archive --prefix "$DIRNAME/" -o "$PARENT/$DIRNAME.tar" "$CHANGESET"

cd "$PARENT"
tar xf "$DIRNAME.tar" && rm "$DIRNAME.tar"

# vim: tabstop=4 expandtab shiftwidth=4
