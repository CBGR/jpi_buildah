#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# This script initialises a container for building and deploying software.

set -e

PROGNAME=$(basename "$0")
THISDIR=$(realpath $(dirname "$0"))
BASEDIR=$(realpath "$THISDIR/..")
DIRNAME=$(basename "$BASEDIR")
APP="/var/www/apps/$DIRNAME"

# Program parameters.

BASE=$1
CONTAINER=$2

# Require a base image and container name.

if [ ! "$BASE" ] || [ "$BASE" = '--help' ] || [ ! "$CONTAINER" ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME <base image> <container>

Initialise a build container using the indicated base image, naming the
container with the specified container name.

For example, to initialise a build container based on CentOS 8:

$PROGNAME centos:8 centos8

Or to initialise a build container based on Almalinux 8:

$PROGNAME almalinux:8 almalinux8

A tag or version (given as 8 in the above examples) should be indicated to
ensure that package repositories are configured correctly.
EOF
    exit 1
fi

# Conveniences based on the parameters.

COPY="buildah copy $CONTAINER"
RUN="buildah run $CONTAINER"

# Create the container from any given base.

buildah from --name "$CONTAINER" "$BASE"

# Initialise extra repositories.

BASE_REPO=$(echo "$BASE" | cut -d: -f1)
BASE_VERSION=$(echo "$BASE" | cut -d: -f2)

# Handle different distributions.

if [ "$BASE_REPO" = 'centos' ] ; then
    $RUN dnf install -y "https://dl.fedoraproject.org/pub/epel/epel-release-latest-${BASE_VERSION:-8}.noarch.rpm"
elif [ "$BASE_REPO" = 'almalinux' ] ; then
    $RUN dnf install -y epel-release
fi

# vim: tabstop=4 expandtab shiftwidth=4
