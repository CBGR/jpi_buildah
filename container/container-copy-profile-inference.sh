#!/bin/sh

# SPDX-FileCopyrightText: 2020-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

set -e

PROGNAME=$(basename "$0")
THISDIR=$(dirname "$0")
BASEDIR=$(realpath "$THISDIR/..")
DIRNAME=$(basename "$BASEDIR")

# Repository details.

PROFILE_INFERENCE_URL="https://github.com/wassermanlab/JASPAR-profile-inference.git"

PI_2022_CHANGESET="5fa64a79c79763384b484d9345bf112bd7dcbf11"
PI_2020_CHANGESET="faa660497f6a222c22f3aa42db992a5f70f84b4b"

# Target details.

TARGET=$1

# Require a target location.

if [ ! "$TARGET" ] || [ "$TARGET" = '--help' ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME <target>

Unpack the profile inference tool to the indicated target location. Typically,
this script is invoked by the more general build script.
EOF
    exit 1
fi

# Cache the repository outside the container.

WORKDIR="$BASEDIR/deploy/work"

if [ ! -e "$WORKDIR" ] ; then
    mkdir "$WORKDIR"
fi

JPI_CACHED="$WORKDIR/jpi"

# Download the package and place it in the base directory.
# The dependencies are incorporated into the JASPAR dependencies found in the
# requirements file, additional ones being pandas, scipy and tqdm.

if [ ! -e "$JPI_CACHED" ] ; then
    git clone "$PROFILE_INFERENCE_URL" "$JPI_CACHED"

# Otherwise, make sure the repository is up to date.

else
    OLDPWD="$PWD"
    cd "$JPI_CACHED"
    git fetch
    cd "$OLDPWD"
fi

# Define paths relative to the target location.

PI="$TARGET/profile-inference"

# Make version-specific clones.

if [ ! -e "$PI/2022/jpi" ] ; then
    "$BASEDIR/deploy/tools/unpack-repo.sh" "$JPI_CACHED" "$PI/2022/jpi" "$PI_2022_CHANGESET"
fi

if [ ! -e "$PI/2020/jpi" ] ; then
    "$BASEDIR/deploy/tools/unpack-repo.sh" "$JPI_CACHED" "$PI/2020/jpi" "$PI_2020_CHANGESET"
fi

# vim: tabstop=4 expandtab shiftwidth=4
