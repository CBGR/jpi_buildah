#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# This script attempts to populate a directory or a container image with the
# profile inference software. However, it does not create a new Buildah image or
# produce an image for use with Docker.

set -e

PROGNAME=$(basename "$0")
THISDIR=$(dirname "$0")
BASEDIR=$(realpath "$THISDIR/..")
DIRNAME=$(basename "$BASEDIR")
PACKAGE='jpi'

# Program parameters.

if [ "$1" = '--use-buildah' ] ; then
    USE_BUILDAH=$1
    shift 1
else
    USE_BUILDAH=
fi

CONTAINER=${1:-"$PACKAGE"}

if [ "$CONTAINER" = '--help' ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME [ --use-buildah ] <container or directory>

Unpack the profile inference software in a directory. If the --use-buildah
option is specified, a container of the given name will be used.
EOF
    exit 1
fi

# Install the profile inference software.

if [ "$USE_BUILDAH" ] ; then
    buildah unshare \
        "$THISDIR/container-build-profile-inference.sh" "$CONTAINER"
else
    TARGET="$BASEDIR/$PACKAGE"
    "$THISDIR/container-copy-profile-inference.sh" "$TARGET"
fi

# vim: tabstop=4 expandtab shiftwidth=4
