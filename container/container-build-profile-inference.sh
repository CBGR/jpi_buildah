#!/bin/sh

# SPDX-FileCopyrightText: 2020-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

set -e

PROGNAME=$(basename "$0")
THISDIR=$(dirname "$0")
BASEDIR=$(realpath "$THISDIR/..")
DIRNAME=$(basename "$BASEDIR")
PACKAGE='jpi'

# Container details.

CONTAINER=$1

# Require a container.

if [ ! "$CONTAINER" ] || [ "$CONTAINER" = '--help' ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME <container>

Deploy the profile inference tool inside the indicated container. Typically,
this script is invoked by the more general build script.
EOF
    exit 1
fi

# Access the container filesystem to copy the repositories.

MNT=$(buildah mount "$CONTAINER")

"$THISDIR/container-copy-profile-inference.sh" "$MNT/var/www/apps/$DIRNAME/$PACKAGE"

buildah umount "$CONTAINER"

# vim: tabstop=4 expandtab shiftwidth=4
