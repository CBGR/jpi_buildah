#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# This script extracts the software built and installed in a container to an
# archive outside the container.

set -e

PROGNAME=$(basename "$0")
THISDIR=$(realpath $(dirname "$0"))
BASEDIR=$(realpath "$THISDIR/..")
DIRNAME=$(basename "$BASEDIR")
PACKAGE='jpi'

# Program parameters.

if [ "$1" = '--use-buildah' ] ; then
    USE_BUILDAH=$1
    shift 1
else
    USE_BUILDAH=
fi

CONTAINER=${1:-"$PACKAGE"}
ARCHIVE=${2:-"$BASEDIR/$PACKAGE.tar.gz"}

if [ "$CONTAINER" = '--help' ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME [ --use-buildah ] <container or directory> [ <archive> ]

Extract installed software from the given container or directory, creating an
archive of the given name, or using a name derived from the package name.
EOF
    exit 1
fi

# Copy installed software from the container.

APPS="/var/www/apps"
APP="$APPS/$DIRNAME"

# Create an archive rooted at $DIRNAME within $APPS containing only the
# profile-inference directory.

if [ "$USE_BUILDAH" ] ; then
    buildah unshare \
        "$BASEDIR/deploy/buildah/buildah-extract.sh" "$CONTAINER" \
            "$APP" "$ARCHIVE" "$PACKAGE"

# For a directory, use the given location instead.

else
    SOURCE="$BASEDIR"
    "$BASEDIR/deploy/tools/archive.sh" "$SOURCE" "$ARCHIVE" "$PACKAGE"
fi

# vim: tabstop=4 expandtab shiftwidth=4
