JASPAR Profile Inference Packaging
==================================

This repository provides tools for packaging the JASPAR profile inference
tools for deployment, potentially in a container.

To install the necessary software to use these tools:

.. code:: shell

  ./deploy.sh

This installs the packages listed in ``requirements-sys.sh``.

Although container management tools are not strictly needed to package this
software, they may still be used, firstly installing them:

.. code:: shell

  ./deploy.sh -p container

This installs the packages listed in ``requirements-sys-container.sh``.

To build and package the profile inference tools without container management
tools being used:

.. code:: shell

  container/container-build.sh jpi
  container/container-package.sh jpi

The above commands respectively prepare the software and package the software
as an archive.

To build and package the profile inference tools for a system or container of
a particular operating system distribution:

.. code:: shell

  container/container-init.sh centos:8 jpi
  container/container-build.sh --use-buildah jpi
  container/container-package.sh --use-buildah jpi

The above commands respectively initialise a build container using a base
image (``centos:8`` indicated here), build the software within the container,
and extract the built software as an archive.

As a result, the following files should be produced:

::

  jpi.tar.gz
  jpi.tar.gz.deps

The first of these is an archive containing the built software. The second is
a manifest of the software's run-time dependencies.

The archive can be unpacked into a system or container of the same type as
that chosen for the base image, and the dependencies manifest can be used to
obtain the software upon which the packaged software depends.
